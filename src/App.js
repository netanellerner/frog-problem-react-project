import './App.css';
import './components/Navbar';
import Navbar from './components/Navbar';
import ShadowBox from './components/ShadowBox';
import HowFrogsDoImage from './components/HowFrogsDoImage';
import Link from './components/Link'
import BuildArrayFrogs from './components/BuildArrayFrogs'
import DevelopedBy from './components/DevelopedBy'


function App() {
    return (
        <div className="App">
            <Navbar/>
            <ShadowBox/>
            <HowFrogsDoImage/>
            <Link/>
            <BuildArrayFrogs></BuildArrayFrogs>
            <DevelopedBy></DevelopedBy>
        </div>
    );
}

export default App;
