import React from 'react';
import FrogBox from '../components/FrogBox';

const greenFrog = '/images/greenFrog.jpg';
const blackFrog = '/images/blackFrog.jpg';

const flexStyle = {
    display: 'flex',
    justifyContent: 'center',
}


let frogsColors = [{
    id: 6,
    img: blackFrog
},
    {
        id: 5,
        img: blackFrog
    },
    {
        id: 4,
        img: blackFrog
    },
    {
        id: 3,
        img: ''
    },
    {
        id: 2,
        img: greenFrog
    },
    {
        id: 1,
        img: greenFrog
    },
    {
        id: 0,
        img: greenFrog
    }];

export default function BuildArrayFrogs() {

    return (<div id="frogsArray" style={flexStyle}>
        {
            frogsColors.map(frog =>
                <FrogBox id={frog.id} img={frog.img} alt={`Frog ${frog.id}`}></FrogBox>)
        }
    </div>)
}
