import React from 'react';
import {useState} from 'react';


let frogArrTests = ["green", "green", "green", "white", "black", "black", "black"];
const frogArrWinningTest = ["green", "green", "green", "white", "black", "black", "black"];

const boxStyle = {
    backgroundColor: 'white',
    width: '100px',
    height: '100px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '50px',
    border: 'red solid 2px',
};

const imageStyle = {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '50%'
}

export default function FrogBox({id, img}) {

    //this function gonna update the arrayFrogs
    const updateArrFrogs = (index1, index2) => {

    }

    //this function gonna Check if the array is arranged
    //in the form of a puzzle solution
    const isWinArray = () => {
        let result = true;

        for (let idx = 0; idx < frogArrTests.length; idx++) {
            if (frogArrTests[idx] !== frogArrWinningTest[idx]) {
                result = false;
                break;
            }
        }

        if (result) {
            //צבע את כל הריבועים בירוק ותודיע על ניצחון
            (Object.values(document.getElementById("frogsArray").childNodes))
                .forEach(child => child.style.backgroundColor = "green");

            alert("ניצחת!")
        }

        return result;
    }
    
    const FrogCanMoveIsOk = () =>{

    }

    const whereFrogCanMove = (evt) => {
        const frogId = evt.target.id;

        (Object.values(document.getElementById("frogsArray").childNodes))
            .forEach(child => child.style.backgroundColor = "white");

        if (frogId) {
            document.getElementById(frogId).style.backgroundColor = "green";
            if (frogArrTests[parseInt(frogId) + 1] === 'white') {
                document.getElementById(parseInt(frogId) + 1).style.backgroundColor = "#33ccff";
            } else if (frogArrTests[parseInt(frogId) + 2] === 'white') {
                document.getElementById(parseInt(frogId) + 2).style.backgroundColor = "#33ccff";
            } else if (frogArrTests[parseInt(frogId) - 1] === 'white') {
                document.getElementById(parseInt(frogId) - 1).style.backgroundColor = "#33ccff";
            } else if (frogArrTests[parseInt(frogId) - 2] === 'white') {
                document.getElementById(parseInt(frogId) - 2).style.backgroundColor = "#33ccff";
            } else {
                document.getElementById(parseInt(frogId)).style.backgroundColor = "#ffffff";
            }
        }
    }

    return (
        <div style={boxStyle} id={id} onClick={isWinArray}>
            <img src={img} style={imageStyle}/>
        </div>);
}
