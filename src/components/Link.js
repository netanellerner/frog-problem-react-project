import React from 'react';


const tryStyle= {
  textDecoration: 'none'
  };

 
  
  
  export default function Link() {
    return (<div >
                <h2 className="text-danger">נסו לפתור את החידה במספר קטן ביותר של מהלכים:</h2>
                <a 
                style={tryStyle}
                href="https://ymath.haifa.ac.il/images/stories/mispar_chazak_2000/issue21/lev-zamir.pdf"
        >
                לפתרון החידה לחץ כאן</a>

    </div>)
}
