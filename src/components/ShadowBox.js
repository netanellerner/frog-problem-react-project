import React from 'react';

const shadowboxParent = {
    width: '50%',
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'space-between',
    display: 'flex'
};

const shadowbox = {
    flexDirection: 'row',
    marginBlockEnd: '10px',
    width: '15em',
    border: '1px solid #333',
    boxShadow: '8px 8px 5px #444',
    padding: '8px 12px',
    backgroundImage: 'linear-gradient(180deg, #fff, #ddd 40%, #ccc)'
  };

  export default function ShadowBox() {
    return (<div>

<div style={shadowboxParent}>
        <div style={shadowbox}>
            <p>אפשר להקפיץ את הצפרדע
                השחורה מעל הצפרדע
                השכנה, אל משבצת ריקה
            </p>
        </div>
        <div style={shadowbox}>
            <p>אפשר להחליק את
                הצפרדע הירוקה
                למשבצת הסמוכה
            </p>
        </div>
        </div>

    </div>)
}