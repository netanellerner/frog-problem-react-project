import React from 'react';

const howFrogsDo = '/images/howFrogsDo.jpg';

const imageStyle = {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '50%'
}


export default function howFrogsDoImage() {
    return (<div>
        <img src={howFrogsDo} style={imageStyle}></img>
    </div>)
}