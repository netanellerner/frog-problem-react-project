import React from 'react';

const footerStyle= {
    color: '#3b0012',
    display: 'table',
    marginLeft: 'auto',
    marginRight: 'auto'
  };

  export default function developedBy() {
    return (<div >
                <footer style={footerStyle} class="text-center">פותח על ידי: נתנאל לרנר</footer>
    </div>)
}
