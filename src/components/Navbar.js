import React from 'react';
const frogsHowDo = '/images/frogsHowDo.jpg';
const frogLikeThat = '/images/frogLikeThat.jpg';

const imageStyle = {
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: '50%'
};

const h3Style = {
  color: 'maroon'
};

export default function Navbar() {
  return (<div>
    
    <h1 className="text-center text-danger">בעיית הצפרדעים</h1>
    <p>לפניכם שורה של 7 משבצות. שלוש הצפרדעים הירוקות בצידה השמאלי ושלוש הצפרדעים השחורות, בצידה
        הימני. בין שתי הקבוצות, משבצת ריקה.</p>
        <img src={frogsHowDo} style={imageStyle}></img>
    <p>המשימה שלכם, להחליף בין המקומות של הצפרדעים, באופן, שכל הצפרדעים הירוקות תעבורנה לצדה הימני
        של השורה והשחורות תעבורנה לצידה השמאלי.<br/><br/>כמו בתמונה הבאה:</p>
        <img src={frogLikeThat} style={imageStyle}></img>
    <h3 style={h3Style}>תנאי ההעברות:</h3>
    <ul>
        <li>כל העברה מתבצעת על צפרדע יחידה (לא ניתן בו זמנית להעביר יותר מצפרדע אחת).</li>
        <li>הצפרדעים הירוקות יכולות לנוע לכיוון ימין בלבד, והצפרדעים השחורות יכולות לנוע לכיוון שמאל בלבד.</li>
        <li>ניתן להזיז צפרדע למשבצת ריקה הסמוכה אליה.</li>
        <li>ניתן להעביר צפרדע באמצעות קפיצה מעל צפרדע אחרת (בתנאי שהמשבצת אליה מעבירים אותה ריקה).</li>
    </ul>
        <p>לדוגמה:</p>
  </div>)
}
